from aiohttp import web
import socket

name = socket.gethostname()

async def handle(request):
    text = "Hello from " + name
    return web.Response(text=text)

app = web.Application()
app.add_routes([web.get('/', handle)])

if __name__ == '__main__':
    web.run_app(app)