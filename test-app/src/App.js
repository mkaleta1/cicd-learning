import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          CI/CD on GitLab
        </p>
      </header>
    </div>
  );
}

export default App;
